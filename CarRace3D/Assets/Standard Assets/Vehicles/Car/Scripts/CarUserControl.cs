using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Events;


namespace UnityStandardAssets.Vehicles.Car
{
    //[RequireComponent(typeof (ButtonScript))] (Removed the additional script ButtonScript attached,
    //hence not using it here)
    [RequireComponent(typeof (CarController))]
    
    public class CarUserControl : MonoBehaviour
    {
        
        public CarController m_Car; // the car controller we want to use
        public Button forwardButton, backwardButton, leftButton, rightButton;
        public bool front = false;
        public bool back = false;
        public bool left = false;
        public bool right = false;
        public void Awake()
        {
            // get the car controller
            m_Car = GetComponent<CarController>();
        }

        void Start()
        {
            front = false;
            back = false;
            left = false;
            right = false;

            Button btn1 = forwardButton.GetComponent<Button>();
            Button btn2 = backwardButton.GetComponent<Button>();
            Button btn3 = leftButton.GetComponent<Button>();
            Button btn4 = rightButton.GetComponent<Button>();

            //Calls the TaskOnClick/TaskWithParameters method when you click the Button
            btn1.onClick.AddListener(ForwardButtonClick);
            btn2.onClick.AddListener(BackwardButtonClick);
            btn3.onClick.AddListener(LeftButtonClick);
            btn4.onClick.AddListener(RightButtonClick);
        }

        void ForwardButtonClick()
        {         
            back = false;
            left = false;
            right = false;
            front = true;
        }

        void BackwardButtonClick()
        {
            front = false;
            right = false;
            left = false;
            Button btn2 = backwardButton.GetComponent<Button>();
            btn2.onClick.AddListener(ReverseButtonClick);           
        }

        void ReverseButtonClick()
        {
            back = true;
        }

        void LeftButtonClick()
        {
            right= false;
            front = false;
            back = false;
            left = true;
        }

        void RightButtonClick()
        {
            left = false;
            front = false;
            back = false;
            right = true;     
            
        }


        public void FixedUpdate()
        { 
            if(front)
            {
                m_Car.Move(0.0f, 0.6f, 0.0f, 0.0f);
            }
            else
            {
                m_Car.Move(0.0f, 0.0f, 0.0f, 0.0f);
            }
            if(back)
            {
                m_Car.Move(0.0f, 0.0f, -0.1f, 0.0f);
            }
            if(left)
            {
                m_Car.Move(-0.5f, 0.3f, 0.0f, 0.0f);
            }
            if(right)
            {
                m_Car.Move(0.5f, 0.3f, 0.0f, 0.0f);
            }
/*           // pass the input to the car!
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetButtonDown("ArrowUp");
            float r = CrossPlatformInputManager.GetButtonDown("ArrowDown");
//            float handbrake = CrossPlatformInputManager.GetAxis("Jump");
//#if !MOBILE_INPUT        
//            m_Car.Move(h, v, r, handbrake);
//#else
//            m_Car.Move(h, v, r, 0f);
//#endif           */
        }
    }
}
