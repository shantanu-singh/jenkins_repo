﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine.Events;

public class ReveiveInputFromUI : MonoBehaviour
{
   // public GameObject Car;
    public bool front = false;
  public CarController controlCar;

    public void Awake()
    {
        // get the car controller
        controlCar = GetComponent<CarController>();


    }
    // Use this for initialization
    void Start()
    {
        front = false;
    }

    public void OnPointerDown()
    {
        front = true;
    }
    public void OnPointerUp()
    {
        front = false;
    }


    // Update is called once per frame
   public void Update()
    {
        if (front)
        {
            controlCar.Move(0.0f, 0.6f, 0.0f, 0.0f);
        }
        else
        {
            controlCar.Move(0.0f, 0.0f, 0.0f, 0.0f);
        }
    }

}