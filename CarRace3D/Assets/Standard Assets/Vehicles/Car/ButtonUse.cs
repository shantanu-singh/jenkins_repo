﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityStandardAssets.Vehicles.Car;

public class ButtonUse : MonoBehaviour
{
    public CarController controlCar;

    void Start()
    { 
        controlCar = GetComponent<CarController>();
    }

    public void OnForwardButtonDown(PointerEventData data)
    {
        controlCar.Move(0.0f, 0.1f, 0.0f, 0.0f);
    }

    public void OnForwardButtonUp()
    {
        controlCar.Move(0.0f, 0.1f, 0.0f, 0.0f);
    }

    public void OnBackwardButtonDown()
    {

    }
}
