﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.Vehicles.Car;

public class ButtonControlForCar : MonoBehaviour
{
    // public GameObject Car;   
    public CarController controlCar;
    //public bool front = false;
    //public bool back = false;
    //public bool left = false;
    //public bool right = false;
    float directionControl;
    float speedControl;

    public void Awake()
    {
        // get the car controller
        controlCar = GetComponent<CarController>();
    }
    // Use this for initialization
    void Start()
    {
        //front = false;
        //back = false;
        //left = false;
        //right = false;
        directionControl = 0.0f;
        speedControl = 0.0f;
    }

    public void OnForwardButtonDown()
    {
        //front = true;
        speedControl = 0.75f;
        //controlCar.Move(0.0f, 0.75f, 0.0f, 0.0f); changed to update parameters inside update funtion
    }
    public void OnForwardButtonUp()
    {
        //front = false;
        speedControl = 0.0f;
    }

    public void OnReverseButtonDown()
    {
        //back = true;
        speedControl = -0.4f;
    }

    public void OnReverseButtonUp()
    {
       // back = false;
        speedControl = 0.0f;
    }

    public void OnLeftButtonDown()
    {
        //left = true;
        directionControl = -0.5f;
    }

    public void OnLeftButtonUp()
    {
        //left = false;
        directionControl = 0.0f;
    }

    public void OnRightButtonDown()
    {
        //right = true;
        directionControl = 0.5f;
    }

    public void OnRightButtonUp()
    {
       // right = false;
        directionControl = 0.0f;
    }


    // Update is called once per frame
    public void FixedUpdate()
    {
        //float directionControl = 0.0f, speedControl = 0.0f;

        //if(front)
        //{
            
        //}
        //else if(back)
        //{
        //    speedControl = -0.4f;
        //}
        //else
        //{
        //    speedControl = 0.0f;
        //}

        //if(left)
        //{
        //    sideControl = -0.5f;
        //}
        //else if(right)
        //{
        //    sideControl = 0.5f;
        //}
        //else
        //{
        //    sideControl = 0.0f;
        //}

        controlCar.Move(directionControl, speedControl, speedControl, 0.0f);
        //if (front)
        //{
        //    controlCar.Move(0.0f, 0.75f, 0.0f, 0.0f);
        //}
        //else if (back)
        //{
        //    controlCar.Move(0.0f, 0.0f, -0.4f, 0.0f);
        //}
        //else if (left)
        //{
        //    controlCar.Move(-0.5f, 0.0f, 0.0f, 0.0f);
        //}
        //else if (right)
        //{
        //    controlCar.Move(0.5f, 0.0f, 0.4f, 0.0f);
        //}
        //else
        //{
        //    controlCar.Move(0.0f, 0.0f, 0.0f, 0.0f);
        //}
    }

}