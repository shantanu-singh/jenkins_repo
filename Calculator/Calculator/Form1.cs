﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        Double value = 0;
        String operation = "";
        bool operatorPressed = false;

        public Form1()
        {
            InitializeComponent();
        }
        

        private void button_Click(object sender, EventArgs e)
        {
            if ( (displayBox.Text == "0") || (operatorPressed) )
            {
                displayBox.Clear();
                operatorPressed = false;
            }
            Button btn = (Button)sender;         
            displayBox.Text = displayBox.Text + btn.Text;
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            displayBox.Text = "0";
        }

        private void operator_Click(object sender, EventArgs e)
        {
            Button btn = (Button) sender;
            operation = btn.Text;   
            Double.TryParse(displayBox.Text,out value);
            operatorPressed = true;
        }

        private void buttonEquals_Click(object sender, EventArgs e)
        {            
            switch (operation)
            {
                case "+":
                    displayBox.Text = (value + Double.Parse(displayBox.Text)).ToString();
                    break;
                case "-":
                    displayBox.Text = (value - Double.Parse(displayBox.Text)).ToString();
                    break;
                case "*":
                    displayBox.Text = (value * Double.Parse(displayBox.Text)).ToString();
                    break;
                case "/":
                    displayBox.Text = (value / Double.Parse(displayBox.Text)).ToString();
                    break;
                default:
                    break;
            }
            operatorPressed = false;
            value = Double.Parse(displayBox.Text);
            operation = "";
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            displayBox.Clear();
            value = 0;
        }

        private void buttonDecimal_Click(object sender, EventArgs e)
        {
            if (!displayBox.Text.Contains("."))
                displayBox.Text += ".";
            else
                return;
        }

        private void buttonPlusMinus(object sender, EventArgs e)
        {
            if (!displayBox.Text.Contains("-"))
                displayBox.Text = "-" + displayBox.Text;
            else
                displayBox.Text = ""+ displayBox.Text;
        }

  
    }
}
